
Description
-----------
This module provides an enhanced way to view the watchdog log.

1) checkboxes at top to select message type(s)

2) mouseover to view details

3) flush watchdog of messages of selected types

4) restrict date range



Installation
------------
1. copy the watchspy directory and all its contents to your 
   modules directory.
2. enable the module: admin/build/modules
3. configure the module: admin/settings/watchspy


Usage
-------------
After enabling the module, the watchspy report can be viewed at: 
admin/logs/watchspy

You may select one or more message types to display; if noe are checked, all are 
displayed.

You may also select a range of dates to display.

Below the list of message types, there are links to check or uncheck all types.

There is a "Purge" checkbox below these links. If checked; all messages of the 
selected type(s) will be removed from the watchdog log. Use with cuation, as 
this is permanent. It is useful during testing when you want to start with a 
clean slate.


Configuration
-------------
Configuration settings are at admin/settings/watchspy

There are three options:

  1) Number of rows to display on each report page. Default=25; set lower for 
  faster page views, higher for less navigating.
  
  2) Display details when hovering over an item. Default=true; turn off if you 
  don't want the hover feature and/or to reduce page load times.
  
  3) Number of columns for message type checkboxes. Default = 2. Set to a 
  smaller number if you have a narrow display area.


Styling
------- 
Presentation is controlled in watchspy.css; you may overwrite these styles in 
your template's style sheet.


Known Issues
-------------
None.


Author
------
Phil Glatz <drupal@glatz.com>
www.glatz.com

Initial module development sponsored by Consumer Search, a service of About.com, 
a part of The New York Times Company.
http://www.consumersearch.com
