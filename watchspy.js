
if (Drupal.jsEnabled) {
  $(document).ready(function() {
   // click on "filter by all messages" button
   $('#tf-checkall').click(
      function() {
        $("#watchspy-form-overview").checkCheckboxes('#edit-purge-log');
      });
   $('#tf-clearall').click(
      function() {
        $('input[@type=checkbox]').removeAttr('checked');
      });
  
   // click on display row to expand details
   $('tr.watchspy-showme').click(
      function() {
        var ival = $(this).attr('id').match(/\d+$/);
        $('#watchspy-detail-' + ival).show();
      });
  
   // hover on display summary row to expand details
   $('tr.watchspy-showme').hover(
      function() {
        var ival = $(this).attr('id').match(/\d+$/);
        $('#watchspy-detail-' + ival).show();
      },
      function() {
        var ival = $(this).attr('id').match(/\d+$/);
        $('#watchspy-detail-' + ival).hide();
      }
      );
  });

  /**
   * Check all checkboxes contained within a form
   *
   * @name     checkCheckboxes
   * @param    ignore  Checkboxes to ignore
   * @author   Sam Collett (http://www.texotela.co.uk)
   * @example  $("#myform").checkCheckboxes();
   * @example  $("#myform").checkCheckboxes("[@name=ignorethis]");
   * 
   * from http://www.texotela.co.uk/code/jquery/checkboxes/1/
   *
   */
  jQuery.fn.checkCheckboxes = function(ignore)
  {
    ignore = ignore || [];
    return this.each(
      function()
      {
        jQuery("input[@type=checkbox]", this).not(ignore).each(
          function()
          {
            this.checked = true;
          }
        )
      }
    )
  }

}



